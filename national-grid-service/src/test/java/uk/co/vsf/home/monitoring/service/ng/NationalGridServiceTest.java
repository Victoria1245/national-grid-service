package uk.co.vsf.home.monitoring.service.ng;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import uk.co.vsf.home.monitoring.service.Application;
import uk.co.vsf.home.monitoring.service.ng.domain.NationalGridCarbonStatus;
import uk.co.vsf.home.monitoring.service.ng.domain.NationalGridSystemData;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { Application.class })
@TestPropertySource(locations = "classpath:/test.properties")
public class NationalGridServiceTest {

	@Autowired
	private NationalGridService nationalGridService;

	@Test
	public void getCurrentCarbonStatus() {
		NationalGridCarbonStatus status = nationalGridService.getCurrentCarbonStatus();
		assertNotNull(status);
		
		assertThat(status.toString(), containsString("WPD East Midlands"));
		assertThat(status.toString(), containsString("\"postcode\":\"CV13\""));
	}

	@Test
	public void getSystemData() {
		NationalGridSystemData systemData = nationalGridService.getSystemData();
		assertNotNull(systemData);
		
		assertThat(systemData.toString(), containsString("time"));
		assertThat(systemData.toString(), containsString("demand"));
		assertThat(systemData.toString(), containsString("frequency"));
	}
}
