package uk.co.vsf.home.monitoring.service.ng.domain;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

public class NationalGridSystemDataTest {

    @Test
    public void diagnostics_Demand() throws IOException {
        InputStream is = getClass().getResourceAsStream("/system-data-example.html");
        String input = IOUtils.toString(is, StandardCharsets.UTF_8);

        int firstTableStart = input.indexOf("<table");
        int firstTableEnd = input.indexOf("</table>");

        System.out.println(String.format("firstTableStart %d, firstTableEnd %d", firstTableStart, firstTableEnd));

        String firstTable = input.substring(firstTableStart, firstTableEnd);

        System.out.println(firstTable);

        for (int i = 0; i < 10; i++) {
            firstTable = firstTable.replaceAll("  ", " ");
        }
        System.out.println(firstTable);
    }

    @Test
    public void diagnostics_Interconnectors() throws IOException {
        InputStream is = getClass().getResourceAsStream("/system-data-example.html");
        String input = IOUtils.toString(is, StandardCharsets.UTF_8);

        int firstTableEnd = input.indexOf("</table>");
        int secondTableStart = input.indexOf("<table", firstTableEnd);
        int secondTableEnd = input.indexOf("</table>", secondTableStart);

        System.out.println(String.format("secondTableStart %d, secondTableEnd %d", secondTableStart, secondTableEnd));

        String secondTable = input.substring(secondTableStart, secondTableEnd);

        System.out.println(secondTable);

        for (int i = 0; i < 10; i++) {
            secondTable = secondTable.replaceAll("  ", " ");
        }
        secondTable = secondTable.replaceAll(StringUtils.CR, "");
        secondTable = secondTable.replaceAll(StringUtils.LF, "");
        System.out.println(secondTable);

        Pattern pattern = Pattern.compile("<Center> [\\w|\\s]+ </Center> </td> <td> <Center> [-|\\d]+ MW </Center>");
        Matcher matcher = pattern.matcher(secondTable);
        while (matcher.find()) {
            String interconnectorMatch = matcher.group();

            Pattern interconnectorDetailsPattern = Pattern.compile(
                    "<Center> ([\\w|\\s]+) </Center> </td> <td> <Center> ([-|\\d]+) MW </Center>");
            Matcher interconnectorDetailsMatcher = interconnectorDetailsPattern.matcher(interconnectorMatch);

            interconnectorDetailsMatcher.groupCount();
            if (interconnectorDetailsMatcher.find()) {
                String name = interconnectorDetailsMatcher.group(1);
                String value = interconnectorDetailsMatcher.group(2);
                System.out.println(String.format("name '%s', value '%s'", name, value));
            }
        }
    }

    @Test
    public void extractData() throws IOException {
        InputStream is = getClass().getResourceAsStream("/system-data-example.html");
        String input = IOUtils.toString(is, StandardCharsets.UTF_8);

        NationalGridSystemData sd = new NationalGridSystemData(input);
        System.out.println(sd.toString());
        
        assertEquals(20874, sd.getDemand().intValue());
        assertEquals(new BigDecimal("50.06").setScale(2), sd.getFrequency().setScale(2));
        assertEquals("13:55:00", sd.getTime().format(DateTimeFormatter.ISO_LOCAL_TIME));
    }
}
