package uk.co.vsf.home.monitoring.service.ng;

import uk.co.vsf.home.monitoring.service.ng.domain.NationalGridCarbonStatus;

public interface NationalGridCarbonStatusService {

	NationalGridCarbonStatus getCurrentStatus();
}
