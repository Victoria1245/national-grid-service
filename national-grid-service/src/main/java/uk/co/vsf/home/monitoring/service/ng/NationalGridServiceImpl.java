package uk.co.vsf.home.monitoring.service.ng;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import uk.co.vsf.home.monitoring.service.ng.domain.NationalGridCarbonStatus;
import uk.co.vsf.home.monitoring.service.ng.domain.NationalGridSystemData;

@Service
public class NationalGridServiceImpl implements NationalGridService {

	private static final Logger LOGGER_SDTOUT = LogManager.getLogger("stdoutonly");
	private static final Logger LOGGER = LogManager.getLogger();

	private final NationalGridCarbonStatusService nationalGridCarbonStatusService;
	private final NationalGridRealtimeService nationalGridRealtimeService;

	public NationalGridServiceImpl(@Autowired NationalGridCarbonStatusService nationalGridCarbonStatusService,
			@Autowired NationalGridRealtimeService nationalGridRealtimeService) {
		this.nationalGridCarbonStatusService = nationalGridCarbonStatusService;
		this.nationalGridRealtimeService = nationalGridRealtimeService;
	}

	@Scheduled(cron = "0 5,35 * * * *")
	@Override
	public NationalGridCarbonStatus getCurrentCarbonStatus() {
        LOGGER_SDTOUT.info("BEGIN getCurrentCarbonStatus");
		NationalGridCarbonStatus status = nationalGridCarbonStatusService.getCurrentStatus();
		LOGGER_SDTOUT.info("getCurrentCarbonStatus + " + status);
		LOGGER.info(status);
		return status;
	}

	@Scheduled(cron = "10 * * * * *")
	@Override
	public NationalGridSystemData getSystemData() {
        LOGGER_SDTOUT.info("BEGIN getSystemData");
		NationalGridSystemData systemData = nationalGridRealtimeService.getSystemData();
		LOGGER_SDTOUT.info("getSystemData + " + systemData);
		LOGGER.info(systemData);
		return systemData;
	}
}
