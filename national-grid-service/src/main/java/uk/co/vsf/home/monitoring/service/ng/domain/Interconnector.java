package uk.co.vsf.home.monitoring.service.ng.domain;

import java.math.BigDecimal;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class Interconnector {

    private String name;
    private BigDecimal value;

    public Interconnector(String name, String value) {
        this.name = name;
        this.value = new BigDecimal(value);
    }
    
    @Override
    public String toString() {
        return new ReflectionToStringBuilder(this).toString();
    }

    public String getName() {
        return name;
    }

    public BigDecimal getValue() {
        return value;
    }
}
