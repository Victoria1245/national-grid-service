package uk.co.vsf.home.monitoring.service.ng.domain;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Intensity {

	@JsonProperty("forecast")
	private int forecast;

	@JsonProperty("index")
	private String index;

	public int getForecast() {
		return forecast;
	}

	public void setForecast(int forecast) {
		this.forecast = forecast;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	@Override
	public String toString() {
		ReflectionToStringBuilder rtsb = new ReflectionToStringBuilder(this);
		return rtsb.toString();
	}
}
