package uk.co.vsf.home.monitoring.service.ng;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import uk.co.vsf.home.monitoring.service.ng.domain.NationalGridSystemData;

@Service
public class NationalGridRealtimeServiceApi implements NationalGridRealtimeService {

	private final RestTemplate restTemplate;

	@Value("${url.national.grid.realtime.systemdata}")
	private String nationalGridRealtimeSystemDataUrl;

	public NationalGridRealtimeServiceApi(@Autowired RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public NationalGridSystemData getSystemData() {
		String response= this.restTemplate.getForObject(nationalGridRealtimeSystemDataUrl, String.class);
		return new NationalGridSystemData(response);
	}

}
