package uk.co.vsf.home.monitoring.service.ng;

import uk.co.vsf.home.monitoring.service.ng.domain.NationalGridCarbonStatus;
import uk.co.vsf.home.monitoring.service.ng.domain.NationalGridSystemData;

public interface NationalGridService {

	NationalGridCarbonStatus getCurrentCarbonStatus();

	NationalGridSystemData getSystemData();
}