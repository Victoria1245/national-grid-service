package uk.co.vsf.home.monitoring.service.ng.domain;

import static org.apache.commons.lang3.StringUtils.SPACE;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class NationalGridSystemData {

    private static final String DOUBLE_SPACE = SPACE + SPACE;
    private static final String TABLE_START = "<table";
    private static final String TABLE_END = "</table>";
    private LocalTime time;
    private BigInteger demand;
    private BigDecimal frequency;

    private List<Interconnector> interconnectors = new ArrayList<>();

    public NationalGridSystemData(String response) {
        for (int i = 0; i < 10; i++) {
            response = response.replaceAll(DOUBLE_SPACE, SPACE);
        }

        List<String> tables = extractTables(response);

        extractDemandFromResponse(tables.get(0));
        extractFrequencyFromResponse(tables.get(0));

        extractInterconnectors(tables.get(1));

        extractTimeFromResponse(response);
    }

    private void extractInterconnectors(String table) {
        table = table.replaceAll(StringUtils.CR, StringUtils.EMPTY);
        table = table.replaceAll(StringUtils.LF, StringUtils.EMPTY);

        Pattern pattern = Pattern.compile("<Center> [\\w|\\s]+ </Center> </td> <td> <Center> [-|\\d]+ MW </Center>");
        Matcher matcher = pattern.matcher(table);

        while (matcher.find()) {
            String interconnectorMatch = matcher.group();

            Pattern interconnectorDetailsPattern = Pattern.compile(
                    "<Center> ([\\w|\\s]+) </Center> </td> <td> <Center> ([-|\\d]+) MW </Center>");
            Matcher interconnectorDetailsMatcher = interconnectorDetailsPattern.matcher(interconnectorMatch);

            interconnectorDetailsMatcher.groupCount();
            if (interconnectorDetailsMatcher.find()) {
                String name = interconnectorDetailsMatcher.group(1);
                String value = interconnectorDetailsMatcher.group(2);
                interconnectors.add(new Interconnector(name, value));
            }
        }
    }

    private List<String> extractTables(String response) {
        int firstTableStart = response.indexOf(TABLE_START);
        int firstTableEnd = response.indexOf(TABLE_END);
        int secondTableStart = response.indexOf(TABLE_START, firstTableEnd);
        int secondTableEnd = response.indexOf(TABLE_END, secondTableStart);

        String firstTable = response.substring(firstTableStart, firstTableEnd);
        String secondTable = response.substring(secondTableStart, secondTableEnd);

        return List.of(firstTable, secondTable);
    }

    private void extractDemandFromResponse(String response) {
        Pattern pattern = Pattern.compile("(\\d+) MW");
        Matcher matcher = pattern.matcher(response);

        if (matcher.find()) {
            this.demand = new BigInteger(matcher.group(1));
        }
    }

    private void extractFrequencyFromResponse(String response) {
        Pattern pattern = Pattern.compile("(\\d\\d\\.\\d\\d) Hz");
        Matcher matcher = pattern.matcher(response);

        if (matcher.find()) {
            this.frequency = new BigDecimal(matcher.group(1));
        }
    }

    private void extractTimeFromResponse(String response) {
        Pattern pattern = Pattern.compile("(\\d\\d:\\d\\d:\\d\\d)");
        Matcher matcher = pattern.matcher(response);

        if (matcher.find()) {
            this.time = LocalTime.parse(matcher.group(1));
        }
    }

    @Override
    public String toString() {
        return new ReflectionToStringBuilder(this).toString();
    }

    public LocalTime getTime() {
        return time;
    }

    public BigInteger getDemand() {
        return demand;
    }

    public BigDecimal getFrequency() {
        return frequency;
    }

    public List<Interconnector> getInterconnectors() {
        return interconnectors;
    }
}
