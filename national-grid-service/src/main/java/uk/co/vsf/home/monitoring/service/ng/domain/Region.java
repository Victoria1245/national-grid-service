package uk.co.vsf.home.monitoring.service.ng.domain;

import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Region {

	@JsonProperty("regionid")
	private int regionId;

	@JsonProperty("dnoregion")
	private String dnoRegion;

	@JsonProperty("shortname")
	private String shortName;

	@JsonProperty("postcode")
	private String postcode;

	@JsonProperty("data")
	private List<Data> data;

	public int getRegionId() {
		return regionId;
	}

	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}

	public String getDnoRegion() {
		return dnoRegion;
	}

	public void setDnoRegion(String dnoRegion) {
		this.dnoRegion = dnoRegion;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public List<Data> getData() {
		return data;
	}

	public void setData(List<Data> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		ReflectionToStringBuilder rtsb = new ReflectionToStringBuilder(this);
		return rtsb.toString();
	}
}
