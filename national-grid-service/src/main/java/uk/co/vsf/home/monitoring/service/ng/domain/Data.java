package uk.co.vsf.home.monitoring.service.ng.domain;

import java.time.ZonedDateTime;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


public class Data {

	@JsonDeserialize(using = ZonedDateTimeDeserializer.class)
	@JsonSerialize(using = ZonedDateTimeSerializer.class)
	@JsonProperty("from")
	private ZonedDateTime from;

	@JsonDeserialize(using = ZonedDateTimeDeserializer.class)
	@JsonSerialize(using = ZonedDateTimeSerializer.class)
	@JsonProperty("to")
	private ZonedDateTime to;

	@JsonProperty("intensity")
	private Intensity intensity;

	@JsonProperty("generationmix")
	private List<GenerationType> generationmix;

	public ZonedDateTime getFrom() {
		return from;
	}

	public void setFrom(ZonedDateTime from) {
		this.from = from;
	}

	public ZonedDateTime getTo() {
		return to;
	}

	public void setTo(ZonedDateTime to) {
		this.to = to;
	}

	public Intensity getIntensity() {
		return intensity;
	}

	public void setIntensity(Intensity intensity) {
		this.intensity = intensity;
	}

	public List<GenerationType> getGenerationmix() {
		return generationmix;
	}

	public void setGenerationmix(List<GenerationType> generationmix) {
		this.generationmix = generationmix;
	}

	@Override
	public String toString() {
		ReflectionToStringBuilder rtsb = new ReflectionToStringBuilder(this);
		return rtsb.toString();
	}
}
