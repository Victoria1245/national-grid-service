package uk.co.vsf.home.monitoring.service.ng.domain;

import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NationalGridCarbonStatus {

	@JsonProperty("data")
	private List<Region> regions;

	@Override
	public String toString() {
		return new ReflectionToStringBuilder(this).toString();
	}
}
