package uk.co.vsf.home.monitoring.service.ng;

import uk.co.vsf.home.monitoring.service.ng.domain.NationalGridSystemData;

public interface NationalGridRealtimeService {

	NationalGridSystemData getSystemData();
}
