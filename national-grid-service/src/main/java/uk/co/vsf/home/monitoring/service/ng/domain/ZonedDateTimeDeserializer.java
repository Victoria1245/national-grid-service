package uk.co.vsf.home.monitoring.service.ng.domain;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class ZonedDateTimeDeserializer extends JsonDeserializer<ZonedDateTime> {

	@Override
	public ZonedDateTime deserialize(JsonParser arg0, DeserializationContext arg1)
			throws IOException, JsonProcessingException {
		return ZonedDateTime.parse(arg0.getText().replaceAll("\\.\\d*", ""),
				DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mmX"));
	}

}
