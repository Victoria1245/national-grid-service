package uk.co.vsf.home.monitoring.service.ng;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import uk.co.vsf.home.monitoring.service.ng.domain.NationalGridCarbonStatus;

@Service
public class NationalGridCarbonStatusServiceApi implements NationalGridCarbonStatusService {

	private final RestTemplate restTemplate;

	@Value("${url.national.grid.carbon.status}")
	private String carbonStatusUrl;

	public NationalGridCarbonStatusServiceApi(@Autowired RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public NationalGridCarbonStatus getCurrentStatus() {
		return this.restTemplate.getForObject(carbonStatusUrl, NationalGridCarbonStatus.class);
	}
}
