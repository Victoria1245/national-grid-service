FROM amazoncorretto:17

COPY national-grid-service/target/national-grid-service.jar /app/

RUN echo "Europe/London" > /etc/timezone

ENTRYPOINT java -Dlog4j2.formatMsgNoLookups=true -jar /app/national-grid-service.jar