# national-grid-service #

## About this repository ##

This repository (https://bitbucket.org/Victoria1245/national-grid-service/) contains a project which will build a jar that when running will periodically call out to the National Grid's realtime system data endpoint (https://extranet.nationalgrid.com/Realtime/Home/SystemData) and every 30 minutes, the Carbon Intensity endpoint (https://api.carbonintensity.org.uk/regional/postcode/<postcoderegion>).

The purpose of the repo is to take the information from these two sources and send them to a Splunk TCP listener so that the data can then be graphed as the user wishes.

## License ##

[LICENSE](https://bitbucket.org/Victoria1245/national-grid-service/src/trunk/LICENSE)

## How can I run this application? ##

Before running this application, it is recommended to setup your Splunk instance to receive on a TCP input so that the application can start sending data without issues.

### Configuring a Splunk TCP input ###

#### indexes.conf ####

First add an index to your Splunk instance, e.g.

```
[national_grid_service]
coldPath = $SPLUNK_DB/national_grid_service/colddb
enableDataIntegrityControl = 0
enableTsidxReduction = 0
homePath = $SPLUNK_DB/national_grid_service/db
maxTotalDataSizeMB = 80400
thawedPath = $SPLUNK_DB/national_grid_service/thaweddb
```

#### inputs.conf ####

Then add a TCP input, e.g.

```
[tcp://9510]
connection_host = dns
index = national_grid_service
sourcetype = json_no_timestamp
```

### Running the national-grid-service ###

Once you have setup a Splunk index for the data and configured the TCP input, you can then startup the docker container with the national-grid-service application inside.

Change the Splunk host and port as appropriate and select the right postcode region that you would like carbon intensity data for, e.g. PE2 shown below.

```
docker run \
    -d \
    --restart always \
    --memory=256m \
    -e national_grid_postcode_region=CV13 \
    -e SPLUNK_HOST=<splunk-host> \
    -e SPLUNK_PORT=9510 \
    --name national-grid-service \
    vdocker123/national-grid-service:19

docker logs -f national-grid-service
```

The above commands will also start tailing the logs of the container, so press `ctrl + c` to stop the tail.

## Simple Splunk Search ##

Once you've started the docker container, you can then go back in to Splunk and see if the data is being received by running the following query `index=national_grid_service`.  If the docker container is able to successfully call out to get data from the Internet and send it onwards to Splunk, you should see events in the Splunk search.
